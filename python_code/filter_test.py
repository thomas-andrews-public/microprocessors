import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

import filter

PI = np.pi

A = 5
N_samples = 1000
N_periods = 10
f = 110
R = 1.2e3
C = 1e-6

T = 1 / f * N_periods
t = np.linspace(0, T, N_samples)
V_in = A * signal.sawtooth(2 * PI * f * t, 0.5)
V_out_LP = filter.lowpass_RC(V_in, t, R, C)
V_out_HP = filter.lowpass_RC(V_in, t, R, C)

fig1, ax1 = plt.subplots()
ax1.set_ylabel("Voltage (V)")
ax1.set_xlabel("Time (ms)")
ax1.plot(t * 1e3, V_in, label="Voltage In")
ax1.plot(t * 1e3, V_out_LP, label="Low-pass Output")
ax1.grid()
ax1.legend()

fig1, ax1 = plt.subplots()
ax1.set_ylabel("Voltage (V)")
ax1.set_xlabel("Time (ms)")
ax1.plot(t * 1e3, V_in, label="Voltage In")
ax1.plot(t * 1e3, V_out_HP, label="High-pass Output")
ax1.grid()
ax1.legend()


freq_range = [1, 2e3]
N_freqs = 1000
freqs = np.linspace(*freq_range, N_freqs)
gain_lp = []
gain_hp = []

for f in freqs:
    T = 1 / f * N_periods
    t = np.linspace(0, T, N_samples)

    V_in = A * signal.sawtooth(2 * PI * f * t, 0.5)
    V_out = filter.lowpass_RC(V_in, t, R, C)
    A_out = max(V_out)
    gain_lp.append(A_out / A)

    V_in = A * signal.sawtooth(2 * PI * f * t, 0.5)
    V_out = filter.highpass_RC(V_in, t, R, C)
    A_out = max(V_out)
    gain_hp.append(A_out / A)

fig1, ax1 = plt.subplots()
ax1.set_ylabel("Gain")
ax1.set_xlabel("Frequency (Hz)")
ax1.plot(freqs, gain_lp, label="Low-pass")
ax1.grid()

plt.show()
