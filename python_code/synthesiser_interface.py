import serial


def generate_amp_dict(A_max=16384, A_min=255, timestep=16e-6, max_step=256):
    """
    Generate the dictionary of frequencies to their related amplitude step sizes
    """
    amp_dict = {0: 0}

    for step in range(1, max_step):
        amp = 0
        amps = []
        while amp < A_max:
            amp += step
            amps.append(amp)
        amp = A_max
        while amp > A_min:
            amp -= step
            amps.append(amp)
        amp = A_min

        freq = 1 / (timestep * len(amps))
        amp_dict[freq] = step

    return amp_dict


def generate_note_dict(A4=440, step_range=(-24, 3)):
    """Create a dictionary of a440 notes to their frequencies"""
    notes = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
    all_notes = {}
    for n in range(*step_range):
        note = notes[n % 12] + f"{(n + 24) // 12 + 2}"
        all_notes[A4 * 2 ** (1 / 12 * n)] = note
    return all_notes


def closest_freq(amps, note):
    """Return the closest frequency available to the synthesizer to a given a440 note"""
    close_freq, delta_amp = min(amps.items(), key=lambda x: abs(note - x[0]))
    return close_freq, delta_amp


def setup_serial(port, baud_rate):
    ser = serial.Serial(port, baud_rate)
    ser.close()
    return ser


def send_serial(ser: serial.Serial, data):
    ser.open()
    ser.write(data.to_bytes(1, "little"))
    ser.close()


if __name__ == "__main__":
    AMP_MAX = 16384
    AMP_MIN = 255
    TIME_STEP = 15.5e-6
    MAXSTEP = 256

    SER_PORT = "COM4"
    SER_BAUD_RATE = 9600

    amp_dict = generate_amp_dict(AMP_MAX, AMP_MIN, TIME_STEP, MAXSTEP)
    notes = generate_note_dict()
    close_freqs = []
    for note in notes.keys():
        key = notes[note]
        closest_frequency, delta_amplitude = closest_freq(amp_dict, note)
        print(
            f"key: {key}, with frequency: {note}, has closest available frequency: {closest_frequency}, "
            f"given by almplitude step: {delta_amplitude}"
        )
        close_freqs.append(closest_frequency)
