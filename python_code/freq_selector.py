from python_code.synthesiser_interface import *

try:
    import tkinter as tk
except ImportError:
    import Tkinter as tk

if __name__ == "__main__":
    """
    Create a python TKinter GUI with 4 sliders allowing users to set
    the four frequencies produced by the synthesizer manually
    """
    AMP_MAX = 16384
    AMP_MIN = 255
    TIME_STEP = 15.65e-6
    MAXSTEP = 256
    SER_PORT = "COM4"
    SER_BAUD_RATE = 9600

    amp_dict = generate_amp_dict(AMP_MAX, AMP_MIN, TIME_STEP, MAXSTEP)
    valuelist = amp_dict.keys()
    ser = setup_serial(SER_PORT, SER_BAUD_RATE)

    def valuecheck0(value):
        newvalue = closest_freq(amp_dict, int(value))[0]
        slider0.set(newvalue)

    def valuecheck1(value):
        newvalue = closest_freq(amp_dict, int(value))[0]
        slider1.set(newvalue)

    def valuecheck2(value):
        newvalue = closest_freq(amp_dict, int(value))[0]
        slider2.set(newvalue)

    def valuecheck3(value):
        newvalue = closest_freq(amp_dict, int(value))[0]
        slider3.set(newvalue)

    root = tk.Tk()

    slider0 = tk.Scale(
        root, from_=max(valuelist), to=min(valuelist), command=valuecheck0, length=len(valuelist)
    )
    slider1 = tk.Scale(
        root, from_=max(valuelist), to=min(valuelist), command=valuecheck1, length=len(valuelist)
    )
    slider2 = tk.Scale(
        root, from_=max(valuelist), to=min(valuelist), command=valuecheck2, length=len(valuelist)
    )
    slider3 = tk.Scale(
        root, from_=max(valuelist), to=min(valuelist), command=valuecheck3, length=len(valuelist)
    )

    slider0.grid(
        column=0,
        row=1,
    )
    slider1.grid(
        column=1,
        row=1,
    )
    slider2.grid(
        column=2,
        row=1,
    )
    slider3.grid(
        column=3,
        row=1,
    )

    slider_label0 = tk.Label(root, text="Channel 0:")
    slider_label0.grid(
        column=0,
        row=0,
    )
    slider_label1 = tk.Label(root, text="Channel 1:")
    slider_label1.grid(
        column=1,
        row=0,
    )
    slider_label2 = tk.Label(root, text="Channel 2:")
    slider_label2.grid(
        column=2,
        row=0,
    )
    slider_label3 = tk.Label(root, text="Channel 3:")
    slider_label3.grid(
        column=3,
        row=0,
    )

    slider0.set(min(valuelist))
    slider1.set(min(valuelist))
    slider2.set(min(valuelist))
    slider3.set(min(valuelist))

    def send(code):
        send_serial(ser, code)
        print(code)

    def update_freqs():
        send(closest_freq(amp_dict, slider0.get())[1])
        send(closest_freq(amp_dict, slider1.get())[1])
        send(closest_freq(amp_dict, slider2.get())[1])
        send(closest_freq(amp_dict, slider3.get())[1])

    button = tk.Button(root, text="Update Frequencies", command=update_freqs)
    button.grid(
        column=2,
        row=2,
    )

    root.mainloop()
