from scipy.fft import fft, fftfreq
from matplotlib import pyplot as plt
import numpy as np


def FT(y, x):
    """
    Perform a fast fourier transform on given a given set
    of evenly spaced data points x and y
    """
    N = len(x)
    h = x[1] - x[0]

    yf = fft(y)
    xf = fftfreq(N, h)

    return yf, xf


def PlotFT(y, x):
    """
    Plot the fourier transform of the given data set
    """
    yf, xf = FT(y, x)

    fig1, (ax1, ax2) = plt.subplots(2, 1)
    ax1.set_ylabel("Amplitude")
    ax1.set_xlabel("Time (s)")
    ax1.plot(x, y, label="Signal")
    ax1.grid()
    ax1.legend()

    ax2.set_ylabel("Amplitude")
    ax2.set_xlabel("Frequency (Hz)")
    ax2.plot(xf, abs(yf), label="Fourier-Transform")
    ax2.grid()
    ax2.legend()

    plt.show()


def PlotSuperposition(freqs, x):
    sum = np.empty_like(x)

    for freq in freqs:
        amp = np.array(np.sin(2 * np.pi * freq * x))
        sum = sum + amp

    fig1, ax = plt.subplots()
    ax.set_ylabel("Amplitude")
    ax.set_xlabel("Time (s)")
    ax.plot(x, sum, label="Superposition")
    ax.grid()
    ax.legend()

    print(f"Frequencies: {freqs}")
    plt.show()


if __name__ == "__main__":
    N = 1000
    x = np.linspace(0, 10, N)
    y = np.sin(2 * np.pi * x)

    x, y = np.loadtxt(
        "python_code/waveforms/CMaj2.csv", delimiter=",", skiprows=1, unpack=True
    )
    PlotFT(y, x)

    # freqs = (15, 50)
    # PlotSuperposition(freqs, x)
