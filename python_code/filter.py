import numpy as np


def lowpass_RC(V_in, t, R, C):
    # Source: https://en.wikipedia.org/wiki/Low-pass_filter#Simple_infinite_impulse_response_filter
    dt = t[1] - t[0]
    N_samples = len(V_in)

    a = dt / (R * C + dt)
    V_out = [a * V_in[0]]

    for i in range(1, N_samples):
        V = V_out[i - 1] + a * (V_in[i] - V_out[i - 1])
        V_out.append(V)

    return V_out


def highpass_RC(V_in, t, R, C):
    # Source: https://en.wikipedia.org/wiki/High-pass_filter#Algorithmic_implementation
    dt = t[1] - t[0]
    N_samples = len(V_in)

    a = R * C / (R * C + dt)
    V_out = [V_in[0]]

    for i in range(1, N_samples):
        V = a * (V_out[i - 1] + V_in[i] - V_in[i - 1])
        V_out.append(V)

    return V_out
