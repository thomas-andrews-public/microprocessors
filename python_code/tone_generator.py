import numpy as np
import scipy.io.wavfile as wav
import matplotlib.pyplot as plt

PI = np.pi
SAMPLE_RATE = 44100
AMP = np.iinfo(np.int16).max


def generate_tone(freq, length):
    t = np.linspace(0, length, length * SAMPLE_RATE)
    return np.array(AMP * np.sin(2 * PI * freq * t))


def save_tone(filename, data: np.ndarray):
    wav.write(filename, SAMPLE_RATE, data.astype(np.int16))


if __name__ == "__main__":
    length = 2
    notes = {"A4": 440}

    for note, freq in notes.items():
        t = np.linspace(0, length, length * SAMPLE_RATE)
        tone = generate_tone(freq, length)
        save_tone(f"tones/{note}.wav", tone)
