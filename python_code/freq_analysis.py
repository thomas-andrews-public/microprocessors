import synthesiser_interface as synth

if __name__ == '__main__':
    """
    Iterate through all available amplitude steps, 
    printing the corresponding frequency, for data collection
    """
    notes = synth.generate_note_dict(A4=440)
    amp_dict = synth.generate_amp_dict(timestep=15.5E-6)
    ser = synth.setup_serial('COM4', baud_rate=9600)

    for k, v in notes.items():
        amp = amp_dict[synth.closest_freq(amp_dict, k)[0]]
        synth.send_serial(ser, amp)
        synth.send_serial(ser, 0)
        synth.send_serial(ser, 0)
        synth.send_serial(ser, 0)
        input(f"Playing note {v}. \nPress any key to continue...")