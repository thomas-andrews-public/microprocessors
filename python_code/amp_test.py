"""Constants determined by the assembly code"""
FREQUENCY = 1 / 16e-6
AMP_MAX = 16384
AMP_MIN = 255
TIME_STEP = 16e-6
MAXSTEP = 256


def amp_step_to_freq():
    """
    Calculate the frequency that will be returned
    by each amplitude step size, given in hexadecimal
    """
    for step in range(1, MAXSTEP):
        amp = 0
        amps = []
        while amp < AMP_MAX:
            amp += step
            amps.append(amp)
        amp = AMP_MAX
        while amp > AMP_MIN:
            amp -= step
            amps.append(amp)
        freq = 1 / (TIME_STEP * len(amps))
        print(f"amp_step: {hex(step)}, freq: {freq}")


# Press the green button in the gutter to run the script.
if __name__ == "__main__":
    amp_step_to_freq()
