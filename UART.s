#include <xc.inc>
    
global  UART_Setup, UART_Listen
extrn	delta_amp0, delta_amp1, delta_amp2, delta_amp3

    
psect	udata_acs	    ; reserve data space in access ram
UART_counter: ds    1	    ; reserve 1 byte for variable UART_counter
UART_delta_amp0: ds 1

psect	uart_code,class=CODE
UART_Setup:
    bsf	    SPEN	; enable
    bcf	    SYNC	; asynchronous
    bcf	    BRGH	; slow speed
    bcf	    TXEN	; disable transmit
    bsf	    CREN
    bcf	    BRG16	; 8-bit generator
    movlw   103	; gives 9600 Baud rate (actually 9615)
    movwf   SPBRG1, A	; set baud rate
    bsf	    TRISC, PORTC_RX1_POSN, A	; TX1 pin is output on RC6 pin
    bcf	    TRISC, PORTC_TX1_POSN, A	; TX1 pin is output on RC6 pin
					; must set TRISC6 to 1
    return

UART_Listen:
    btfsc   RC1IF
    goto    UART_Update_Channels 
    return
    
UART_Update_Channels:
    movf    RCREG1, W, A    ; if an input is received, move this to the channel 0 amplitude step
    movwf   delta_amp0, A
    bcf	    RC1IF
    
    channel_1_loop:     ; move the next value to channel 1, only move on if process complete (may be interrupted)
    btfss   RC1IF
    bra	    channel_1_loop    
    movf    RCREG1, W, A
    movwf   delta_amp1, A
    bcf	    RC1IF
    
    channel_2_loop:     ; repeat for channel 2 and 3
    btfss   RC1IF
    bra	    channel_2_loop    
    movf    RCREG1, W, A
    movwf   delta_amp2, A
    bcf	    RC1IF

    channel_3_loop:
    btfss   RC1IF
    bra	    channel_3_loop    
    movf    RCREG1, W, A
    movwf   delta_amp3, A
    bcf	    RC1IF

    return
    


