#include <xc.inc>
	
global	DAC_Setup, DAC_Output
extrn	sum_channel_lo, sum_channel_hi

psect	dac_code, class=CODE
    
DAC_Setup:
    bcf	    TRISE, 0, A	    ; Set PIN E0 to output
    bsf	    LATE, 0, A	    ; Hold CS high
    call    SPI_MasterInit
    
SPI_MasterInit:
    bcf	    CKE1
    movlw   (SSP1CON1_SSPEN_MASK) | (SSP1CON1_CKP_MASK) | 0x0; Fix mask to create square clock pulse at Fosc/4 => 16MHz
    movwf   SSP1CON1, A
    bcf	    TRISC, PORTC_SDO1_POSN, A   ; Set up SDO1 on RC5
    bcf	    TRISC, PORTC_SCK1_POSN, A   ; Set up CK1 on RC3
    bcf	    TRISC, 0, A
    bsf	    LATC, 0, A

    return
	
SPI_MasterTransmit:
    movwf   SSP1BUF, A
    call    Wait_Transmit
    return
	
Wait_Transmit:
    btfss   SSP1IF
    bra	    Wait_Transmit
    bcf	    SSP1IF
    return

DAC_Output:     ; write current amplitude value to the hardware as a voltage value
    bcf	    LATE, 0, A	; Hold CS low
    movlw   0x00	; Write instruction
    call    SPI_MasterTransmit
    movf    sum_channel_hi, W
    call    SPI_MasterTransmit
    movf    sum_channel_lo, W
    call    SPI_MasterTransmit
    bsf	    LATE, 0, A
    return