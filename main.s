#include <xc.inc>

extrn	update_channels, initialize_channels, decr0, decr1, decr2, decr3
extrn	Int_Hi, Timer_Setup
extrn	DAC_Setup, DAC_Output
extrn	UART_Listen, UART_Setup

global delta_amp0, delta_amp1, delta_amp2, delta_amp3

psect	udata_acs	; reserve data space in access ram
    delta_amp0:	    ds 1
    delta_amp1:	    ds 1
    delta_amp2:	    ds 1
    delta_amp3:	    ds 1
    
psect	code, abs
rst:	org	0x0000	; reset vector
	goto	start

int_hi:	org	0x0008	; high vector, no low vector
	goto	Int_Hi
	
	
start:	
    call    DAC_Setup   ; initialise the DAC
    call    UART_Setup  ; set up UART connection
    call    initialize_channels     ; clear all 4 of the frequency channels as a precaution
    movlw   0xFF    ; Start with only the first channel having the highest possible step size, all others are 0
    movwf   delta_amp0
    movlw   0x0
    movwf   delta_amp1
    movlw   0x0
    movwf   delta_amp2
    movlw   0x0
    movwf   delta_amp3
    clrf    decr0   ; clear the decr flags for all channels as a precaution
    clrf    decr1
    clrf    decr2
    clrf    decr3
    call    Timer_Setup     ; start the timer0 interupt
    goto    main

main:
    call UART_Listen    ; listen for new frequencies using the uart connection to a python script
    bra main