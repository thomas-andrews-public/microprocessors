#include <xc.inc>
	
extrn	DAC_Output
extrn	update_channels

    
global	Int_Hi, Timer_Setup

psect	udata_acs   ; named variables in access ram
    cnt_l:	ds 1   ; reserve 1 byte for variable LCD_cnt_l
    cnt_h:	ds 1   ; reserve 1 byte for variable LCD_cnt_h
    cnt_ms:	ds 1   ; reserve 1 byte for ms counter
    
psect	dac_code, class=CODE
	
Int_Hi:	
	btfss	TMR0IF		; check that this is timer0 interrupt
	retfie	f		; if not then return
;	incf	PORTD, F, A
	call    update_channels
	call    DAC_Output
	bcf	TMR0IF		; clear interrupt flag
	retfie	f		; fast return from interrupt

Timer_Setup:
	movlw	11001000B	; Set timer0 to have the fastest possible activation rate
	movwf	T0CON, A
	bsf	TMR0IE
	bsf	GIE
	return

