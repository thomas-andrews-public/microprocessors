	#include <xc.inc>

psect	code, abs
	
main:
	org	0x0
	goto	start

	org	0x100		    ; Main code starts here at address 0x100
start:
	movlw 	0x0
	movwf	TRISC, A	    ; Port C all outputs
	bra 	test
delay:
	movlw 0xff
	movwf 0x20
	call decr1
	return
decr1:
	movlw 0xff
	movwf 0x21
	call decr2
	decfsz 0x20, A
	bra decr1
	return
decr2:
	movlw 0xff
	movwf 0x22
	call decr3
	decfsz 0x21, A
	bra decr2
	return
decr3:
	decfsz 0x22, A
	bra decr3
	return
loop:
	movff 	0x06, PORTC
	call	delay
	incf 	0x06, W, A
test:
	movwf	0x06, A	    ; Test for end of loop condition
	movf	PORTD, W, A
	cpfsgt 	0x06, A
	bra 	loop		    ; Not yet finished goto start of loop again
	goto 	0x0		    ; Re-run program from start

	end	main
