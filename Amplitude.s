#include <xc.inc>

extrn	delta_amp0, delta_amp1, delta_amp2, delta_amp3	
    
global	update_channels, initialize_channels
global  decr0, lo_channel_0, hi_channel_0
global	decr1, lo_channel_1, hi_channel_1
global	decr2, lo_channel_2, hi_channel_2
global	decr3, lo_channel_3, hi_channel_3
global	sum_channel_lo, sum_channel_hi

psect	udata_acs   ; named variables in access ram
    decr0:		ds 1
    decr1:		ds 1
    decr2:		ds 1
    decr3:		ds 1
    lo_channel_0:	ds 1
    hi_channel_0:	ds 1
    lo_channel_1:	ds 1
    hi_channel_1:	ds 1
    lo_channel_2:	ds 1
    hi_channel_2:	ds 1
    lo_channel_3:	ds 1
    hi_channel_3:	ds 1
    sum_channel_lo:	ds 1
    sum_channel_hi:	ds 1


psect amp_code,class=CODE

initialize_channels:    ; clear all amplitude channels as a precaution
    clrf    lo_channel_0
    clrf    hi_channel_0
    clrf    lo_channel_1
    clrf    hi_channel_1
    clrf    lo_channel_2
    clrf    hi_channel_2
    clrf    lo_channel_3
    clrf    hi_channel_3
    clrf    sum_channel_lo
    clrf    sum_channel_hi
    return
    
update_channels:    ; if the relevant decr flag is clear, increment channel by the delta_amp amount
    btfss   decr0, 0
    call    inc_chan0
    btfsc   decr0, 0
    call    dec_chan0
    
    btfss   decr1, 0
    call    inc_chan1
    btfsc   decr1, 0
    call    dec_chan1
    
    btfss   decr2, 0
    call    inc_chan2
    btfsc   decr2, 0
    call    dec_chan2
    
    btfss   decr3, 0
    call    inc_chan3
    btfsc   decr3, 0
    call    dec_chan3
    
    call    sum_channels
    
    return

inc_chan0:  ; increment channel 0 amplitude by the delta amp amount, if this surpasses the max amp set the decr flag
    movf    delta_amp0, W
    addwf   lo_channel_0, F
    movlw   0x00
    addwfc  hi_channel_0, F
    movlw   0x3f
    cpfslt  hi_channel_0
    bsf	    decr0, 0, A
    return

dec_chan0:  ; decrement channel 0 by the delta amp amount, if this < the maximum delta amp amount clear decr flag
    movf    delta_amp0, W
    subwf   lo_channel_0, F
    movlw   0x00
    subwfb  hi_channel_0, F
    movlw   0x00
    cpfsgt  hi_channel_0
    bcf	    decr0, 0, A
    return

inc_chan1:
    movf    delta_amp1, W
    addwf   lo_channel_1, F
    movlw   0x00
    addwfc  hi_channel_1, F
    movlw   0x3f
    cpfslt  hi_channel_1
    bsf	    decr1, 0, A
    return

dec_chan1:
    movf    delta_amp1, W
    subwf   lo_channel_1, F
    movlw   0x00
    subwfb  hi_channel_1, F
    movlw   0x00
    cpfsgt  hi_channel_1
    bcf	    decr1, 0, A
    return

inc_chan2:
    movf    delta_amp2, W
    addwf   lo_channel_2, F
    movlw   0x00
    addwfc  hi_channel_2, F
    movlw   0x3f
    cpfslt  hi_channel_2
    bsf	    decr2, 0, A
    return

dec_chan2:
    movf    delta_amp2, W
    subwf   lo_channel_2, F
    movlw   0x00
    subwfb  hi_channel_2, F
    movlw   0x00
    cpfsgt  hi_channel_2
    bcf	    decr2, 0, A
    return

inc_chan3:
    movf    delta_amp3, W
    addwf   lo_channel_3, F
    movlw   0x00
    addwfc  hi_channel_3, F
    movlw   0x3f
    cpfslt  hi_channel_3
    bsf	    decr3, 0, A
    return

dec_chan3:
    movf    delta_amp3, W
    subwf   lo_channel_3, F
    movlw   0x00
    subwfb  hi_channel_3, F
    movlw   0x00
    cpfsgt  hi_channel_3
    bcf	    decr3, 0, A
    return

sum_channels:
    movff   lo_channel_0, sum_channel_lo
    movf    lo_channel_1, W
    addwf   sum_channel_lo, f
    movff   hi_channel_0, sum_channel_hi
    movf    hi_channel_1, W
    addwfc  sum_channel_hi, f
    movf    lo_channel_2, W
    addwf   sum_channel_lo, f
    movf    hi_channel_2, W
    addwfc  sum_channel_hi, f
    movf    lo_channel_3, W
    addwf   sum_channel_lo, f
    movf    hi_channel_3, W
    addwfc  sum_channel_hi, f
    return
  
